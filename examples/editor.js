var testEditor;

$(function () {
    testEditor = editormd("test-editormd", {
        width: "100%",
        height: 640,
        syncScrolling: "single",
        path: "lib/"
    });

    $("#btn_new").click(function () {
        testEditor.clear();
        testEditor.focus();
    });

    $("#btn_open").click(function () {
        testEditor.setValue('TEST');
        testEditor.focus();
    });

    $("#btn_save").click(function () {
        console.log(testEditor.getValue());
    });

    $("#btn_save_as").click(function () {
        console.log(testEditor.getValue());
    });

    /*
    // or
    testEditor = editormd({
        id      : "test-editormd",
        width   : "90%",
        height  : 640,
        path    : "../lib/"
    });
    */
});